<?php
/**
 * ApiException class.
 */

namespace CivicrmApi;

use Exception;

/**
 * Exception raised when the CiviCRM API returns an error.
 */
class ApiException extends Exception
{

}
