<?php
/**
 * Contact class.
 */

namespace CivicrmApi;

/**
 * Manage CiviCRM contacts.
 */
class Contact extends ApiObject
{
    /**
     * Get the contact's websites.
     *
     * @return Website[]
     */
    public function getWebsites()
    {
        return Website::getAll(['contact_id' => $this->get('id')]);
    }

    /**
     * Get one of the contact's websites.
     *
     * @param WebsiteType $type Website type
     *
     * @return Website
     */
    public function getWebsite(WebsiteType $type)
    {
        try {
            return Website::getSingle(
                [
                    'contact_id' => $this->get('id'),
                    'website_type_id' => $type->id
                ]
            );
        } catch (ApiException $e) {
            return null;
        }
    }

    /**
     * Set one of the contact's websites.
     * @param WebsiteType $type Website type
     * @param string      $url  URL
     */
    public function setWebsite(WebsiteType $type, $url)
    {
        $website = $this->getWebsite($type);

        if (!isset($website)) {
            $website = new Website();
            $website->set('contact_id', $this->get('id'));
            $website->set('website_type_id', $type->id);
        }

        $website->set('url', $url);
        $website->save();
    }

    /**
     * Get all the organizations this contact is a member of.
     * @return self[]
     */
    public function getOrganizations(RelationshipType $type)
    {
        $relationships = Relationship::getAll(
            [
                'contact_id_a' => $this->get('id'),
                'is_active' => true,
                'relationship_type_id' => $type->get('id')
            ]
        );
        $contacts = [];

        foreach ($relationships as $relationship) {
            $organization = new self($relationship->get('contact_id_b'));
            if (!$organization->get('contact_is_deleted')) {
                $contacts[] = $organization;
            }
        }

        return $contacts;
    }

    /**
     * Get all the members of this contact.
     * @return self[]
     */
    public function getMembers(RelationshipType $type)
    {
        $relationships = Relationship::getAll(
            [
                'contact_id_b' => $this->get('id'),
                'is_active' => true,
                'relationship_type_id' => $type->get('id')
            ]
        );
        $contacts = [];

        foreach ($relationships as $relationship) {
            $contacts[] = new self($relationship->get('contact_id_a'));
        }

        return $contacts;
    }

    /**
     * Get the relationship with a specific parent.
     *
     * @param Contact $parent Parent contact
     *
     * @return Relationship
     */
    public function getParentRelationship(Contact $parent, RelationshipType $type)
    {
        return Relationship::getSingle(
            [
                'contact_id_a' => $this->get('id'),
                'contact_id_b' => $parent->get('id'),
                'relationship_type_id' => $type->get('id')
            ]
        );
    }

    /**
     * Get all the tags this contact.
     *
     * @param Tag $tagset Get only tags from this tagset
     *
     * @return Tag[]
     */
    public function getTags(Tag $tagset = null)
    {
        $relationships = EntityTag::getAll(
            [
                'entity_table' => 'civicrm_contact',
                'entity_id' => $this->get('id')
            ]
        );
        $tags = [];

        foreach ($relationships as $relationship) {
            $tag = new Tag($relationship->get('tag_id'));

            if (!isset($tagset) || $tag->get('parent_id') == $tagset->get('id')) {
                $tags[] = $tag;
            }
        }

        return $tags;
    }

    /**
     * Check if this contact has the specified tag.
     *
     * @param Tag $tag Tag
     *
     * @return boolean
     */
    public function hasTag(Tag $tag)
    {
        try {
            EntityTag::getSingle(
                [
                    'entity_table' => 'civicrm_contact',
                    'entity_id' => $this->get('id'),
                    'tag_id' => $tag->get('id')
                ]
            );

            return true;
        } catch (ApiException $e) {
            return false;
        }
    }

    /**
     * Add a new tag to the contact.
     *
     * @param Tag $tag Tag
     *
     * @return void
     */
    public function addTag(Tag $tag)
    {
        if (!$this->hasTag($tag)) {
            $relationship = new EntityTag();
            $relationship->set('entity_table', 'civicrm_contact');
            $relationship->set('entity_id', $this->get('id'));
            $relationship->set('tag_id', $tag->get('id'));
            $relationship->save();
        }
    }

    /**
     * Remove a tag from the contact.
     *
     * @param Tag $tag Tag
     *
     * @return void
     */
    public function removeTag(Tag $tag)
    {
        if ($this->hasTag($tag)) {
            $relationship = EntityTag::getSingle(
                [
                    'entity_table' => 'civicrm_contact',
                    'entity_id' => $this->get('id'),
                    'tag_id' => $tag->get('id')
                ]
            );
            $relationship->delete();
        }
    }

    /**
     * Return the user associated with this contact (if any).
     * @return User
     */
    public function getUser()
    {
        try {
            return User::getSingle(['contact_id' => $this->get('contact_id')]);
        } catch (ApiException $e) {
            // The contact does not have any associated user.
        }
    }

    /**
     * Get a contact with this e-mail address.
     *
     * @param string $email E-mail address
     * @param string $type  Contact type
     *
     * @return self
     */
    public static function getByEmail($email, $type = null)
    {
        return self::getSingle(['email' => $email, 'contact_type' => $type]);
    }

    /**
     * Get the contact corresponding to this user ID.
     *
     * @param int $id User ID
     *
     * @return self
     */
    public static function getByUserId($id)
    {
        $user = new User($id);
        return new self($user->get('contact_id'));
    }

    /**
     * Get the contact with this ID.
     *
     * @param int $id Contact ID
     *
     * @return self
     */
    public static function getById($id)
    {
        return new self($id);
    }

    /**
     * Check if this contact can edit another contact.
     *
     * @param self $contact Other contact
     *
     * @return bool
     */
    public function canEdit(self $contact)
    {
        $relationships = Relationship::getAll(
            [
                'contact_id_a' => $this->get('id'),
                'contact_id_b' => $contact->get('id'),
                'is_active' => true
            ]
        );

        foreach ($relationships as $relationship) {
            if ($relationship->get('is_permission_a_b') == 1) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if this contact is a member of another contact.
     *
     * @param self             $contact Other contact
     * @param RelationshipType $type    Relationship type
     *
     * @return bool
     */
    public function isMember(self $contact, RelationshipType $type)
    {
        $relationships = Relationship::getAll(
            [
                'contact_id_a' => $this->get('id'),
                'contact_id_b' => $contact->get('id'),
                'relationship_type_id' => $type->get('id'),
                'is_active' => true
            ]
        );

        foreach ($relationships as $relationship) {
            return true;
        }

        return false;
    }

    /**
     * Check if this contact is a member the same organization as another contact.
     *
     * @param self             $contact Other contact
     * @param RelationshipType $type    Relationship type
     *
     * @return bool
     */
    public function isComember(self $contact, RelationshipType $type)
    {
        $otherOrganizations = $contact->getOrganizations($type);
        foreach ($this->getOrganizations($type) as $organization) {
            foreach ($otherOrganizations as $otherOrganization) {
                if ($otherOrganization->get('contact_id') == $organization->get('contact_id')) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Does this contact has a member that can edit it?
     *
     * @return boolean
     */
    public function hasAdmin()
    {
        $relationships = Relationship::getAll(
            [
                'contact_id_b' => $this->get('id'),
                'is_active' => true,
                'is_permission_a_b' => 1
            ]
        );

        return !empty($relationships);
    }

    /**
     * Add a relationship
     *
     * @param self             $otherContact Other contact
     * @param RelationshipType $type         Relationship type
     * @param bool             $admin        Can edit the other contact?
     * @param string           $description  Relationship description
     *
     * @return Relationship
     */
    public function addRelationship(self $otherContact, RelationshipType $type, $admin = false, $description = '')
    {
        try {
            $relationship = Relationship::getSingle(
                [
                    'contact_id_a' => $this->get('contact_id'),
                    'contact_id_b' => $otherContact->get('contact_id'),
                    'relationship_type_id' => $type->get('id')
                ]
            );
        } catch (ApiException $e) {
            $relationship = new Relationship();
            $relationship->set('contact_id_a', $this->get('contact_id'));
            $relationship->set('contact_id_b', $otherContact->get('contact_id'));
            $relationship->set('relationship_type_id', $type->get('id'));
        }
        $relationship->set('is_active', true);
        if ($admin) {
            $permission = 1;
        } else {
            $permission = 2;
        }
        $relationship->set('is_permission_a_b', $permission);
        $relationship->set('description', $description);
        $relationship->save();

        return $relationship;
    }

    /**
     * Set the address.
     *
     * @param string       $streetAddress       Street address
     * @param string       $supplementalAddress Supplemental address
     * @param string       $city                City
     * @param string       $postalCode          Postal code
     * @param LocationType $type                Address type
     * @param boolean      $primary             Is it the primary address?
     *
     * @return Address
     */
    public function setAddress(
        $streetAddress,
        $supplementalAddress,
        $city,
        $postalCode,
        LocationType $type,
        Country $country = null,
        StateProvince $province = null,
        $primary = true
    ) {
        try {
            $address = Address::getSingle(
                [
                    'contact_id' => $this->get('contact_id'),
                    'is_primary' => $primary,
                    'location_type_id' => $type->get('id')
                ]
            );
        } catch (ApiException $e) {
            $address = new Address();
            $address->set('contact_id', $this->get('contact_id'));
            $address->set('is_primary', $primary);
            $address->set('location_type_id', $type->get('id'));
        }
        $address->set('street_address', $streetAddress);
        $address->set('supplemental_address_1', $supplementalAddress);
        $address->set('city', $city);
        $address->set('postal_code', $postalCode);
        if (isset($country)) {
            $address->set('country_id', $country->get('id'));
        } else {
            $address->set('state_province_id', '');
        }
        if (isset($province)) {
            $address->set('state_province_id', $province->get('id'));
        } else {
            $address->set('state_province_id', '');
        }
        $address->save();

        return $address;
    }

    /**
     * Set the e-mail address
     *
     * @param string       $address E-mail address
     * @param LocationType $type    E-mail address type
     * @param boolean      $primary Is it the primary e-mail address?
     *
     * @return Email
     */
    public function setEmail($address, LocationType $type, $primary = true)
    {
        if (empty($address) && empty($this->get('email'))) {
            // Avoid an API error.
            return;
        }

        try {
            $email = Email::getSingle(
                [
                    'contact_id' => $this->get('contact_id'),
                    'is_primary' => $primary,
                    'location_type_id' => $type->get('id')
                ]
            );
        } catch (ApiException $e) {
            $email = new Email();
            $email->set('contact_id', $this->get('contact_id'));
            $email->set('is_primary', $primary);
            $email->set('location_type_id', $type->get('id'));
        }
        $email->set('email', $address);
        $email->save();

        return $email;
    }

    /**
     * Set the phone number
     *
     * @param string       $number  Phone number
     * @param LocationType $type    Phone number type
     * @param boolean      $primary Is it the primary phone number?
     *
     * @return Email
     */
    public function setPhone($number, LocationType $type, $primary = true)
    {
        if (empty($number) && empty($this->get('phone'))) {
            // Avoid an API error.
            return;
        }

        try {
            $phone = Phone::getSingle(
                [
                    'contact_id' => $this->get('contact_id'),
                    'is_primary' => $primary,
                    'location_type_id' => $type->get('id')
                ]
            );
        } catch (ApiException $e) {
            $phone = new Phone();
            $phone->set('contact_id', $this->get('contact_id'));
            $phone->set('is_primary', $primary);
            $phone->set('location_type_id', $type->get('id'));
        }
        $phone->set('phone', $number);
        $phone->save();

        return $phone;
    }
}
