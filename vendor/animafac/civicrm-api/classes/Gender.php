<?php
/**
 * Gender class.
 */

namespace CivicrmApi;

use Exception;

/**
 * Manage genders.
 *
 * The CiviCRM API does not have a Gender endpoint so we use a custom object.
 */
class Gender
{

    /**
     * Gender ID.
     *
     * @var int
     */
    public $id;

    /**
     * Gender name.
     *
     * @var string
     */
    public $name;

    /**
     * Gender constructor.
     *
     * @param string $name Type name
     */
    public function __construct($name)
    {
        $api = Api::getInstance();
        foreach ($api->getOptions('Contact', 'gender_id') as $gender) {
            if ($gender->value == $name) {
                $this->id = $gender->key;
                $this->name = $gender->value;
                return;
            }
        }

        throw new Exception("Can't find this gender: ".$name);
    }

    /**
     * Get all available genders.
     * @return self[]
     */
    public static function getAll()
    {
        $api = Api::getInstance();
        $return = [];
        foreach ($api->getOptions('Contact', 'gender_id') as $gender) {
            $return[] = new self($gender->value);
        }

        return $return;
    }
}
