<?php
/**
 * ApiObject class.
 */

namespace CivicrmApi;

use stdClass;

/**
 * Manage CiviCRM objects.
 */
abstract class ApiObject
{
    /**
     * Contact object returned by the CiviCRM API.
     * @var stdClass
     */
    private $apiObject;

    /**
     * Api class instance.
     * @var Api
     */
    protected $api;

    /**
     * List of properties that need to be casted as integer.
     * @var array
     */
    private static $integerProperties = ['id', 'contact_id'];

    /**
     * ApiObject constructor.
     * @param string $field Name of the field containing the ID
     * @param int    $id    Object ID
     */
    public function __construct($id = null)
    {
        $this->api = Api::getInstance();
        if (isset($id)) {
            $this->setApiObject($this->api->getSingle(self::getShortClass(), ['id' => $id]));
        } else {
            $this->setApiObject(new stdClass());
        }
    }

    /**
     * Get a property.
     *
     * @param string $name Name of the property
     *
     * @return mixed Value of the property
     */
    public function get($name)
    {
        if (isset($this->apiObject->$name)) {
            if (in_array($name, self::$integerProperties)) {
                return (int) $this->apiObject->$name;
            }

            return $this->apiObject->$name;
        }
    }

    /**
     * Set a property.
     *
     * The property won't be saved in the database until you call save().
     *
     * @param string $name  Name of the property
     * @param mixed  $value Value of the property
     */
    public function set($name, $value)
    {
        if (in_array($name, self::$integerProperties)) {
            $value = (int) $value;
        }

        $this->apiObject->$name = $value;
    }

    /**
     * Save the object.
     *
     * @return void
     */
    public function save()
    {
        return $this->api->create(self::getShortClass(), $this->apiObject);
    }

    /**
     * Delete the object.
     *
     * @return void
     */
    public function delete()
    {
        $this->api->delete(self::getShortClass(), $this->apiObject);
    }

    /**
     * Get the current class name without any namespace.
     * @return string Class name
     */
    protected static function getShortClass()
    {
        $classname = get_called_class();
        return substr($classname, strrpos($classname, '\\') + 1);
    }

    /**
     * Get all objects from this type.
     * @return ApiObject[]
     */
    public static function getAll(array $constraint = [])
    {
        $api = Api::getInstance();
        $result = [];

        foreach ($api->get(self::getShortClass(), $constraint) as $object) {
            $class = get_called_class();
            $result[] = new $class($object->id);
        }

        return $result;
    }

    /**
     * Get the number of objects from this type.
     * @return int
     */
    public static function getCount(array $constraint = [])
    {
        $api = Api::getInstance();

        return $api->getCount(self::getShortClass(), $constraint);
    }

    /**
     * Get a single object.
     * @return ApiObject[]
     * @throws ApiException When it finds more than one object.
     * @throws ApiException When it can't find any object.
     */
    public static function getSingle(array $constraint = [])
    {
        $api = Api::getInstance();

        $object = $api->getsingle(self::getShortClass(), $constraint);
        $class = get_called_class();

        return new $class($object->id);
    }

    /**
     * Manually set the API object.
     * @param stdClass $object API object
     */
    private function setApiObject(stdClass $object)
    {
        $this->apiObject = $object;
    }
}
