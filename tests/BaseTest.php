<?php
/**
 * BaseTest class.
 */

namespace CivicrmFrontoffice\Test;

use CivicrmApi\Test\BaseTest as ApiTest;
use CivicrmApi\Api;
use PHPUnit\Framework\TestCase;

use Mockery;
use WP_Mock;
use WP_User;

use civicrm_api3;
use stdClass;

/**
 * Class used to create mocks common to every test class.
 */
abstract class BaseTest extends ApiTest
{

    /**
     * Create mock classes.
     */
    protected function setUp()
    {
        // This must always be called first.
        WP_Mock::setUp();
        parent::setUp();

        Mockery::mock('overload:WP_User')
            ->shouldReceive('get');

        WP_Mock::wpFunction('wp_upload_dir');
        WP_Mock::wpFunction('load_plugin_textdomain');
        WP_Mock::wpFunction('plugin_basename');
        WP_Mock::wpFunction('wp_enqueue_style');
        WP_Mock::wpFunction('wp_enqueue_script');
        WP_Mock::wpFunction('plugins_url');
        WP_Mock::wpFunction('add_query_arg');
        WP_Mock::wpFunction('wp_redirect');
        WP_Mock::wpFunction('do_shortcode');
        WP_Mock::wpFunction('get_bloginfo');
        WP_Mock::wpFunction('home_url');
        WP_Mock::wpFunction('plugin_dir_url');
        WP_Mock::wpFunction('shortcode_exists');
        WP_Mock::wpFunction('wp_delete_user');
        WP_Mock::wpFunction('wp_nonce_field');
        WP_Mock::wpFunction('wp_verify_nonce', ['return' => true]);

        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_action']);
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_id']);
        WP_Mock::wpFunction('get_query_var', ['args' => ['civicrm_offset', 0], 'return' => 0]);
        WP_Mock::wpFunction('get_query_var', ['args' => ['civicrm_limit', 10], 'return' => 10]);
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_from']);

        $this->user = new WP_User();
        $this->user->ID = 42;
        WP_Mock::wpFunction('wp_get_current_user', ['return' => $this->user]);

        $this->civicrmApiMock
            ->shouldReceive('getsingle')
                ->with(['id' => 6])
                ->andReturn(true)
            ->shouldReceive('getsingle')
                ->with(['contact_id' => null, 'is_primary' => true, 'location_type_id' => 42])
                ->andReturn(true)
            ->shouldReceive('getsingle')
                ->with(['name' => 'tagset'])
                ->andReturn(true)
            ->shouldReceive('getsingle')
                ->with(['name' => 'invalid_tagset'])
                ->andReturn(false)
            ->shouldReceive('getsingle')
                ->with(['entity_table' => 'civicrm_contact', 'entity_id' => null, 'subject' => 'note'])
                ->andReturn(false)
            ->shouldReceive('getsingle')
                ->with(['contact_id_a' => null, 'contact_id_b' => null, 'relationship_type_id' => 42])
                ->andReturn(false)
            ->shouldReceive('getsingle')
                ->with(['contact_id_a' => null, 'contact_id_b' => 42, 'relationship_type_id' => 42])
                ->andReturn(true)
            ->shouldReceive('getsingle')
                ->with(['id' => 'new_member_id'])
                ->andReturn(true)
            ->shouldReceive('getsingle')
                ->with(['id' => 'province'])
                ->andReturn(true)

            ->shouldReceive('get')
                ->with(
                    [
                        'contact_id_a' => null, 'contact_id_b' => 42,
                        'relationship_type_id' => 42, 'is_active' => true
                    ]
                )
                ->andReturn(true)
            ->shouldReceive('get')
                ->with(['contact_id_a' => null, 'is_active' => true, 'relationship_type_id' => 42])
                ->andReturn(true)
            ->shouldReceive('get')
                ->with(['contact_id_a' => null, 'contact_id_b' => null, 'is_active' => true])
                ->andReturn(true)
            ->shouldReceive('get')
                ->with(['contact_id_a' => 42, 'contact_id_b' => null, 'is_active' => true])
                ->andReturn(true)
            ->shouldReceive('get')
                ->with(['contact_id_a' => null, 'contact_id_b' => 42, 'is_active' => true])
                ->andReturn(true)
            ->shouldReceive('get')
                ->with(
                    [
                        'contact_type' => 'Organization', 'display_name' => 'name',
                        'state_province_id' => 'province', 'do_not_email' => 0, 'tag' => 'tag',
                        'options' => ['offset' => 0, 'limit' => 10, 'sort' => 'sort_name']
                    ]
                )
                ->andReturn(true)
            ->shouldReceive('get')
                ->with(['contact_type' => 'Organization', 'display_name' => 'name'])
                ->andReturn(true)
            ->shouldReceive('get')
                ->with(['contact_type' => 'Individual', 'first_name' => 'first_name', 'last_name' => 'last_name'])
                ->andReturn(true)

            ->shouldReceive('getcount')
                ->with(
                    [
                        'contact_type' => 'Organization', 'display_name' => 'name',
                        'state_province_id' => 'province', 'do_not_email' => 0, 'tag' => 'tag',
                        'options' => ['offset' => 0, 'limit' => 10, 'sort' => 'sort_name']
                    ]
                )
                ->andReturn(true)

            ->shouldReceive('create')
                ->with(['contact_id' => 42, 'is_primary' => true, 'location_type_id' => 42, 'phone' => 'phone'])
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(['contact_id' => 42, 'is_primary' => true, 'location_type_id' => 42, 'email' => 'email'])
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(
                    [
                        'id' => 42, 'is_permission_a_b' => 1, 'street_address' => 'street_address',
                        'supplemental_address_1' => 'street_address', 'city' => 'city',
                        'postal_code' => 'postal_code', 'country_id' => 42, 'state_province_id' => 42
                    ]
                )
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(
                    [
                        'id' => 42, 'is_permission_a_b' => 1, 'street_address' => 'street_address',
                        'supplemental_address_1' => 'street_address', 'city' => 'city',
                        'postal_code' => 'postal_code', 'country_id' => 42, 'state_province_id' => 42,
                        'email' => 'email'
                    ]
                )
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(
                    [
                        'id' => 42, 'is_permission_a_b' => true, 'street_address' => 'street_address',
                        'supplemental_address_1' => 'street_address', 'city' => 'city',
                        'postal_code' => 'postal_code', 'country_id' => 42, 'state_province_id' => 42,
                        'email' => 'email', 'phone' => 'phone'
                    ]
                )
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(
                    [
                        'contact_id_a' => null, 'contact_id_b' => null, 'relationship_type_id' => 42,
                        'is_active' => true, 'is_permission_a_b' => true, 'description' => ''
                    ]
                )
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(['id' => 42, 'contact_id' => 42, 'contact_id_a' => 42, 'is_active' => false])
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(
                    [
                        'id' => 42, 'contact_id' => 42, 'first_name' => 'first_name', 'do_not_phone' => true,
                        'do_not_email' => true
                    ]
                )
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(
                    [
                        'id' => 42, 'contact_id' => 42, 'first_name' => 'first_name', 'do_not_phone' => true,
                        'do_not_email' => true, 'phone' => 'phone', 'email' => 'email'
                    ]
                )
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(
                    [
                        'id' => 42, 'contact_id' => 42, 'first_name' => 'first_name', 'do_not_phone' => true,
                        'do_not_email' => true, 'phone' => 'phone', 'email' => 'email'
                    ]
                )
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(
                    [
                        'id' => 42, 'contact_id' => 42, 'first_name' => 'first_name', 'do_not_phone' => false,
                        'do_not_email' => false
                    ]
                )
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(
                    [
                        'id' => 42, 'contact_id' => 42, 'first_name' => 'first_name', 'do_not_phone' => false,
                        'do_not_email' => false, 'phone' => 'phone'
                    ]
                )
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(['contact_type' => 'Organization', 'organization_name' => 'organization_name'])
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(
                    [
                        'id' => 42, 'street_address' => 'street_address',
                        'supplemental_address_1' => 'supplemental_address_1', 'city' => 'city',
                        'postal_code' => 'postal_code', 'state_province_id' => ''
                    ]
                )
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(
                    [
                        'id' => 42, 'street_address' => 'street_address',
                        'supplemental_address_1' => 'supplemental_address_1', 'city' => 'city',
                        'postal_code' => 'postal_code', 'state_province_id' => '',
                        'email' => 'email'
                    ]
                )
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(
                    [
                        'id' => 42, 'street_address' => 'street_address',
                        'supplemental_address_1' => 'supplemental_address_1', 'city' => 'city',
                        'postal_code' => 'postal_code', 'state_province_id' => '',
                        'email' => 'email', 'phone' => 'phone'
                    ]
                )
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(['organization_name' => 'organization_name'])
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(
                    [
                        'id' => 42, 'contact_id' => 42, 'contact_id_a' => 42,
                        'description' => 'new_member_role', 'is_permission_a_b' => 2
                    ]
                )
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(
                    [
                        'contact_id_a' => null, 'contact_id_b' => null, 'relationship_type_id' => 42,
                        'is_active' => true, 'is_permission_a_b' => 2,
                        'description' => 'new_member_role'
                    ]
                )
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(['is_active' => false])
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(['id' => 42, 'is_active' => false])
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(
                    [
                        'entity_table' => 'civicrm_contact', 'entity_id' => null,
                        'subject' => 'note', 'contact_id' => 0, 'note' => 'note'
                    ]
                )
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(['id' => 42, 'contact_id' => 42, 'do_not_phone' => true, 'do_not_email' => true])
                ->andReturn(true)

            ->shouldReceive('delete')
                ->with(
                    [
                        'id' => 42, 'is_permission_a_b' => true, 'street_address' => 'street_address',
                        'supplemental_address_1' => 'street_address', 'city' => 'city',
                        'postal_code' => 'postal_code', 'country_id' => 42, 'state_province_id' => 42,
                        'email' => 'email', 'phone' => 'phone'
                    ]
                )
                ->andReturn(true);

        $this->civicrmApi = $this->addCivicrmObjects(new civicrm_api3());
    }

    /**
     * Destroy mock classes.
     *
     * @return void
     */
    protected function tearDown()
    {
        parent::tearDown();
        WP_Mock::tearDown();
        Mockery::close();

        unset($_POST);
    }
}
