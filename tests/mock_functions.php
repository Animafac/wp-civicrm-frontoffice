<?php
/**
 * Fake functions used by the tests.
 */

/**
 * Fake function
 * @param  string $string
 * @return string
 */
function __($string)
{
    return $string;
}

/**
 * Fake function
 * @param  string $string
 * @return string
 */
function _n($string)
{
    return $string;
}
