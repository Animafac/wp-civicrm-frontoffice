/*jslint browser: true*/
/*global jQuery, L*/
jQuery(document).ready(
    function () {
        'use strict';

        var $map = jQuery('#map');

        if ($map.length > 0) {
            var map = L.map('map'),
                group = new L.featureGroup().addTo(map),
                markers = $map.data('markers');

            L.tileLayer('http://c.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
                attribution: '&copy; <a target="_blank" rel="noopener" href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);



            jQuery.each(
                markers,
                function (i, item) {
                    var marker = L.marker(item.coord).addTo(map);
                    marker.bindPopup('<b>' + item.name + '</b><br/>' + item.description);
                    marker.addTo(group);

                    if (i >= markers.length - 1) {
                        map.fitBounds(group.getBounds());
                    }
                }
            );
        }
    }
);
