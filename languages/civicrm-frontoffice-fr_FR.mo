��    I      d  a   �      0      1     R     ^     `     n     {     �     �  5   �     �     �  H   �     B     Y     v     �     �     �     �     �     �     �     �     �       
   	  ,        A  %   S  '   y     �  %   �  	   �     �     �     �  2   	     D	     S	     e	     |	     �	     �	     �	     �	  	   �	     �	     �	     �	     �	     �	  #   �	     
     +
     1
     @
  !   U
  ;   w
  /   �
  	   �
     �
  	   �
  Z     8   a  <   �  .   �  )     .   0  M   _  !   �     �     �  r  �  %   L     r     ~     �     �     �     �     �  Z   �     ;     D  c   R     �     �  $   �                  	   7     A     H     W     `  
   t          �  1   �     �  )   �  .        3  (   G     p     t     |     �  V   �               *     A     N     Z     q     �     �     �     �     �  
   �     �  $   �               (     8  %   N  O   t  /   �     �       
     i   !  G   �  H   �  9     2   V  6   �  g   �  ,   (     U     i        )   +         B         #   '   
      3   @   A                    $   !       4   ,   7           =   ;      &   "                   F   (      -                         8   .   %   1   D       I   ?             *       :   H   C           	                      E   /       >      <           5      G             2             6          0      9    %s member found %s members found %s per page : Abbreviation: Add a member Add an organization Add to organization Admin Admins can edit the organization and add new members. Afficher Already a member Are you sure you want to delete your account? This can not be cancelled! Can't find this member Can't find this organization Can't find this relationship City Could not find this action. Create an organization Delete E-mail E-mail address Edit Edit avatar Employee Error: First name Found %s organization Found %s organizations General secretary I agree to being contacted by e-mail. I agree to being contacted on my phone. I changed my mind I confirm I want to delete my account Last name Members No member found No organization found Only admin members can edit the organization info. Open Accordion Organization name Organization nick name Organizations Phone Phone number Please contact Postal code President Role Role of %s in %s Save Search Search for a member Search for an existing organization Send a message State Street address Supplemental address The association doesn't exist yet The first member is automatically promoted to admin status. This account is not linked to any organization. Treasurer Vice-President Volunteer We will remember some information about you in order to unsubscribe you from our mailings. You are not allowed to add members to this organization. You are not allowed to display members of this organization. You are not allowed to edit this organization. You are not allowed to edit this profile. You are not allowed to edit this relationship. You are the first person to join this organization, so you will be its admin. You must be logged in to do this. admin and Project-Id-Version: civicrm-frontoffice
POT-Creation-Date: 2018-12-13 14:38+0100
PO-Revision-Date: 
Last-Translator: Pierre Rudloff <contact@rudloff.pro>
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.8.11
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 %s membre trouvé %s membres trouvés %s par page  : Sigle : Ajouter un membre Ajouter une association Ajouter à l'association Administrateur.rice Les administrateur.rice.s peuvent modifier l'association et y ajouter de nouveaux membres. Afficher Déjà membre Êtes-vous sûr.e de vouloir supprimer votre compte ? Cette opération ne peut pas être annulée ! Membre introuvable Association introuvable Impossible de trouver cette relation Ville Action introuvable. Créer une association Supprimer E-mail Adresse e-mail Modifier Modifier son avatar Salarié.e Erreur : Prénom %s association trouvée %s associations trouvées Secrétaire général.e J'accepte d'être contacté.e par e-mail. J'accepte d'être contacté.e par téléphone. J'ai changé d'avis Je confirme vouloir supprimer mon compte Nom Membres Aucun membre trouvé Aucune association trouvée Seul.e.s les administrateur.rice.s peuvent modifier les informations de l'association. Ouvrir accordion Nom de l'association Sigle de l'association Associations Téléphone Numéro de téléphone Veuillez contacter Code postal Président.e Rôle Rôle de %s dans %s Enregistrer Rechercher Rechercher un membre Rechercher une association existante Envoyer un message Département Adresse postale Complément d'adresse L’association n’existe pas encore La première personne ajoutée est automatiquement nommée administrateur.rice.  Ce compte n'est relié à aucune association.  Trésorier.ère Vice-président.e Volontaire Nous retiendrons certaines informations vous concernant afin de vous désinscrire de nos envois de mails. Vous n'êtes pas autorisé à ajouter des membres à cette association. Vous n'êtes pas autorisé à afficher les membres de cette association. Vous n'êtes pas autorisé à modifier cette association. Vous n'êtes pas autorisé à modifier ce  profil. Vous n'êtes pas autorisé à modifier cette relation. Vous êtes la première personne à ajouter cette association, vous serez donc son administrateur.rice. Vous devez être connecté.e pour faire ça. administrateur.rice et 